-- test file that sends a large blob: test for OPT_INFILESIZE_LARGE
require "curl"

size = 2^32+1

c = curl.easy_init()
c:setopt(curl.OPT_PUT,1)
c:setopt(curl.OPT_URL,"http://127.0.0.1:3000")
c:setopt(curl.OPT_READFUNCTION,function(n) return n, string.rep("X",n) end)
c:setopt(curl.OPT_INFILESIZE_LARGE, size)
c:setopt(curl.OPT_VERBOSE,1)

-- run a black hole
os.execute("nc -l -p 3000 > /dev/null &")
-- wait for nc to bind the socket
os.execute("sleep 3")

-- go!
print("\nluacurl: performing put of size: " .. size .."\n")
c:perform()

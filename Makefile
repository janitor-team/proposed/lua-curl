# TODO: support for abi&libtool, installation of lua-curl.h

LIBNAME=liblua5.1-curl
LIBTOOL=libtool --silent --tag=CC
HEADER=/usr/include/curl/curl.h
PREFIX=/usr/local/
#LUA=lua50
LUA=lua5.1
LUADOC= luadoc
VERSION_INFO=0:0:0

CONSTANTS:= curlopt.h curl_netrcopt.h \
	curl_form.h curl_authopt.h curl_ftpauthopt.h curl_ftpmethods.h
CURL_CFLAGS:= $(shell curl-config --cflags 2>/dev/null) $(shell getconf LFS_CFLAGS)
CURL_LDFLAGS:= $(shell curl-config --libs 2>/dev/null)
REALSO:=$(LIBNAME).so.$(subst :,.,$(VERSION_INFO))

# ------------------------------ lua5.1 stuff ---------------------------------
ifeq "$(LUA)" "lua5.1"
LUA_CFLAGS := $(shell pkg-config lua5.1 --cflags) $(shell getconf LFS_CFLAGS)
# This retrieves the name of the libtool convenience library for Lua
# (e.g. "/usr/lib/liblua5.1.la") used by --mode=link.
LUA_LIBTOOL_S := $(shell pkg-config lua5.1 --variable=libtool_lib)
LUA_LIBTOOL_D := $(LUA_LIBTOOL_S)
# this is the path where you'll eventually install the module
LUA_RPATH:=$(shell pkg-config lua5.1 --define-variable=prefix=$(PREFIX) \
	--variable=INSTALL_CMOD)
# this is the path where you'll eventually install the C header file
LUA_HPATH:=$(shell pkg-config lua5.1 --define-variable=prefix=$(PREFIX) \
	--variable=includedir)/lua5.1
LUA_DPATH:=$(PREFIX)/share/doc/luacurl/
LUA_LPATH:=$(PREFIX)/lib/
OBJS:= lua-curl.lo luabind.lo
endif

# ------------------------------ lua50 stuff ---------------------------------
ifeq "$(LUA)" "lua50"
LUA_CFLAGS := $(shell pkg-config lua50 --cflags) 
LUA_LIBTOOL_D := $(shell pkg-config lualib50 --libs)
LUA_LIBTOOL_S := -llua50 -llualib50
LUA_RPATH:= $(PREFIX)/lib/lua/50/
LUA_HPATH:=$(PREFIX)/include/lua50/
LUA_DPATH:=$(PREFIX)/share/doc/luacurl/
LUA_LPATH:=$(PREFIX)/lib/
#compat-5.1.[ch] are not provided in this package
OBJS:= lua-curl.lo luabind.lo compat-5.1.lo
endif

# ------------------------- Here the Makefile --------------------------------
all: $(LIBNAME).la
constants:$(CONSTANTS)
doc: doc/curl.html
doc/curl.html: lua-curl.luadoc
	cp lua-curl.luadoc doc/curl.lua
	$(LUADOC) -d `pwd`/doc/ `pwd`/doc/*.lua
	rm -f doc/*.lua doc/index.html

%.lo: %.c
	$(LIBTOOL) --mode=compile $(CC) -c -Wall -O2 $(LUA_CFLAGS) $<

# link objects to make static and dynamic libraries.  The .so will be
# left in "./.libs/".  Note that the Lua library and its dependencies are
# not called out on the link line since they are assumed to be part of
# whatever our library is linked to.  We want to avoid duplicate library
# copies, which is a waste of space and can cause run-time problems.
$(LIBNAME).la curl.so: constants $(OBJS)
	$(LIBTOOL) --mode=link $(CC) \
		-rpath $(LUA_RPATH) -o $(LIBNAME).la \
		-version-info $(VERSION_INFO) \
		$(CURL_LDFLAGS) $(OBJS)
	ln -sf ./.libs/$(REALSO) curl.so

# If all went well, we can dynamically load the module into Lua.  The
# following will load the library into the interpreter and call a function.
test: curl.so
	@echo "************************* lua dynamic ***************************"
	$(LUA) -l curl test.lua
	@echo "*****************************************************************"

# install static and dynamic libraries for module to global location
install: $(LIBNAME).la
	mkdir -p $(LUA_RPATH)
	mkdir -p $(LUA_LPATH)
	$(LIBTOOL) --mode=install install $(LIBNAME).la \
		$(LUA_LPATH)/$(LIBNAME).la
	$(LIBTOOL) --finish $(LUA_LPATH)
	cd $(LUA_RPATH);\
		ln -s /$(subst $(DESTDIR),,$(LUA_LPATH))/$(REALSO) curl.so
	mkdir -p $(LUA_HPATH)
	$(LIBTOOL) --mode=install install lua-curl.h $(LUA_HPATH)/lua-curl.h
	mkdir -p $(LUA_DPATH)
	$(LIBTOOL) --mode=install install doc/curl.html $(LUA_DPATH)/curl.html

clean:
	$(RM) -f *.o *.lo *.la *.so app doc/index.html doc/*.lua
	$(RM) -rf ./.libs/
	$(RM) -f $(CONSTANTS) static-stamp dynamic-stamp
	$(RM) -rf win32-*

dist: clean
	DIR=`basename $$PWD`;\
	VERSION=`grep LUACURL_VERSION version.h|cut -d \" -f 2`;\
		cd ..;\
		cp -r $$DIR lua-curl-$$VERSION; \
		tar -cvzf lua-curl_$$VERSION.orig.tar.gz \
		-X $$DIR/exclude-dist lua-curl-$$VERSION;\
		rm -rf lua-curl-$$VERSION

# Constants genereated starting from the cURL headers:

curlopt.h: $(HEADER)
	$(H)cat $(HEADER) | grep "^ *CINIT(" | sed "s/CINIT(/{\"OPT_/" | \
		tr -s "  " " " | sed "s/, /\",CURLOPTTYPE_/" | \
		sed "s/, / + /" | sed "s/),/},/" > curlopt.h

curl_netrcopt.h:$(HEADER)
	$(H)cat $(HEADER) | grep "^ *CURL_NETRC_[A-Z]*," | \
		sed 's/^ *CURL_\(NETRC_[A-Z]*\),.*$$/{"\1", (int)CURL_\1},/' \
		> curl_netrcopt.h

curl_authopt.h:$(HEADER)
	$(H)cat $(HEADER) | grep "CURLAUTH_" | \
		sed "s/#define *CURL/{\"/" | sed "s/ *\/\*.*\*\///" | \
		sed "s/ /\",/" | sed "s/$$/},/" > curl_authopt.h

curl_ftpauthopt.h:$(HEADER)
	$(H)cat /usr/include/curl/curl.h | grep -B10 '} curl_ftpauth' | \
		grep "CURLFTPAUTH_" | \
		grep -v "LAST.*never use" | \
		sed "s/^ *CURL//" | sed "s/^\([^,]*\),.*$$/{\"\1\",CURL\1},/" \
		> curl_ftpauthopt.h

curl_ftpmethods.h:$(HEADER)
	$(H)cat /usr/include/curl/curl.h | grep -B10 '} curl_ftpmethod' | \
		grep "CURLFTPMETHOD_" | \
		grep -v "LAST.*never use" | \
		sed "s/^ *CURL//" | sed "s/^\([^,]*\),.*$$/{\"\1\",CURL\1},/" \
		> curl_ftpmethods.h

curl_form.h: $(HEADER)
	$(H)cat $(HEADER) | grep "^ *CFINIT" | grep -v "CFINIT(NOTHING)" | \
		sed 's/^ *CFINIT(\([^)]*\)),.*$$/{"FORM_\1",CURLFORM_\1},/' \
		> curl_form.h

# win32 stuff

.PHONY: win32-openssl

win32-openssl:
	make clean
	make constants HEADER=/usr/i586-mingw32msvc/include/curl/curl.h
	mkdir -p win32-openssl
	cd win32-openssl; unzip ../lua5_1_4_Win32_dll8_lib.zip
	cd win32-openssl; rm *.dll *.lib
	cd win32-openssl; unzip ../lua5_1_4_Win32_bin.zip
	i586-mingw32msvc-gcc \
		-I win32-openssl/include/ \
		-I . -I /usr/i586-mingw32msvc/include/ \
		-c lua-curl.c -o win32-openssl/lua-curl.o 
	i586-mingw32msvc-gcc -I . \
		-I win32-openssl/include/ \
		-I /usr/i586-mingw32msvc/include/ \
		-c luabind.c -o win32-openssl/luabind.o 
	i586-mingw32msvc-dlltool -e win32-openssl/exports.o -l win32-openssl/lua-curl.lib win32-openssl/*.o
	i586-mingw32msvc-gcc win32-openssl/*.o \
		-o win32-openssl/curl.dll -shared \
		-L /usr/i586-mingw32msvc/bin/ \
		win32-openssl/lua5.1.dll \
		-lcurl-4
	for X in libcurl-4 libz; do\
		cp /usr/i586-mingw32msvc/bin/$$X.dll win32-openssl;\
	done
	cp test.lua win32-openssl
	rm -rf win32-openssl/*.o win32-openssl/include/ \
		win32-openssl/*.lib win32-openssl/lua51.dll
	i586-mingw32msvc-strip win32-openssl/*.dll
	cp -r win32-openssl freepops-lua-curl-binary-win32-openssl
	zip -r freepops-lua-curl-binary-win32-openssl.zip freepops-lua-curl-binary-win32-openssl
	rm -rf freepops-lua-curl-binary-win32-openssl
	mv freepops-lua-curl-binary-win32-openssl.zip ..


# eof
